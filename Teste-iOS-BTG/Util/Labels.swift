//
//  Labels.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 05/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import Foundation
class Labels{
    static let share = Labels()
    let conservador = "CONSERVADOR"
    let moderado = "MODERADO"
    let sofisticado = "SOFISTICADO"
    let dozeMeses = "12 MESES"
    let aplicacaoInicial = "APLICAÇÃO INICIAL (RS)"
    let resgateEm = "RESGATE EM"
    let mesAtual = "MÊS ATUAL"
    let ano = "ANO"
    let inicioDoFundo = "INÍCIO DO FUNDO"
    let patrimonioLiquido = "PATRIMÔNIO LÍQUIDO (R$)"
    let detalhes = "DETALHES"
    let aplicar = "APLICAR"
    let filtrar = "FILTRAR"
    let filtrarSuaBusca = "REFINE SUA BUSCA"
    let todos = "Todos(as)"
    let nenhum = "Nenhum"
    let categoria = "CATEGORIA"
    let aplicacaoMaxima = "APLICAÇÃO MÍNIMA"
    let gestor = "GESTOR"
    let ordenadoPor = "ORDENADOR POR"
    
}
