//
//  formatDate.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 08/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import Foundation
import UIKit
extension String{
    
    func formatStringDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: self){
            print(date)
            
            let displayFormater = DateFormatter()
            displayFormater.locale = Locale(identifier: "pt_BR")
            displayFormater.dateStyle = .long
            return displayFormater.string(from: date)
        }
        return self
    }
}
