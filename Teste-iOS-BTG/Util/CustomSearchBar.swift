//
//  CustomSearch.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 05/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import Foundation
import  UIKit

extension UITextField{
    
    func setBottonLine(lineColor: UIColor){
        self.borderStyle = UITextField.BorderStyle.none
        self.backgroundColor = UIColor.clear
        
        let borderLine = UIView()
        let height = 1.0
        borderLine.frame = CGRect(x: 0, y: Double(self.frame.height) - height, width: Double(self.frame.width), height: height)
        
        borderLine.backgroundColor = lineColor
        self.addSubview(borderLine)
        

    }
}
