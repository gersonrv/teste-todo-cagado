//
//  Funds.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 04/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import UIKit

struct Funds: Decodable{
    let id: Int
    let product: String
    let description: String
    let type: String
    let riskLevel: Int
    let riskName: String
    let investedBalance: Int
    let availableAccountBalance:  Int?
    let minimumInitialInvestment: Int
    let timeLimit: String
    let profitability:  Rentabilidade
    let tipoInvestimentoIndicador: String
    let detail: Detalhe
    let investment: Investimento
    let assetCode: Int
    let netEquity: Double
    let averageNetEquity:  Int?
    let begin: String
    let inAtivo: Bool
    let signed: Bool
    let scheduled: Bool
    let selected: Bool
    let issuerName: String
    let issuerId: Int
    let quotaRule: String?
    let externalIssuer: String
    let isRecentFund: Bool
    let nextDateUtil:  Int?
    let existOperation: Bool
    let scheduledRedemptionValue:  Int?
    let descriptionFund:  String?
    let signDocuments:  Int?
    let fundsBenchmarks:  Int?
    let graphs:  Int?
    let profitabilityHistory:  Int?
    let showDetail: Bool
    let grantLevel: Bool
    let isBlackList: Bool
    let isWhiteList: Bool
    let isPCO: Bool
    let sameOperation: Bool
    
    private enum CodingKeys: String, CodingKey {
        case id =  "id"
        case product =  "product"
        case description =  "description"
        case type =  "type"
        case riskLevel =  "riskLevel"
        case riskName =  "riskName"
        case investedBalance =  "investedBalance"
        case availableAccountBalance = "availableAccountBalance"
        case minimumInitialInvestment =  "minimumInitialInvestment"
        case timeLimit =  "timeLimit"
        case profitability =  "profitability"
        case tipoInvestimentoIndicador =  "tipoInvestimentoIndicador"
        case detail = "detail"
        case investment = "investment"
        case assetCode =  "assetCode"
        case netEquity = "netEquity"
        case averageNetEquity = "averageNetEquity"
        case begin =  "begin"
        case inAtivo =  "inAtivo"
        case signed =  "signed"
        case scheduled =  "scheduled"
        case selected =  "selected"
        case issuerName =  "issuerName"
        case issuerId =  "issuerId"
        case quotaRule = "quotaRule"
        case externalIssuer =  "externalIssuer"
        case isRecentFund =  "isRecentFund"
        case nextDateUtil = "nextDateUtil"
        case existOperation =  "existOperation"
        case scheduledRedemptionValue = "scheduledRedemptionValue"
        case descriptionFund = "descriptionFund"
        case signDocuments = "signDocuments"
        case fundsBenchmarks = "fundsBenchmarks"
        case graphs = "graphs"
        case profitabilityHistory = "profitabilityHistory"
        case showDetail =  "showDetail"
        case grantLevel =  "grantLevel"
        case isBlackList =  "isBlackList"
        case isWhiteList =  "isWhiteList"
        case isPCO =  "isPCO"
        case sameOperation =  "sameOperation"
        
    }
}
struct  Rentabilidade: Decodable{
    let day: Double
    let lastMonth: Double?
    let month: Double
    let year: Double?
    let twelveMonths: Double?
    let cotaLastMonth: Bool
}
struct Detalhe: Decodable {
    let minimumInitialInvestment: Int
    let minimumMoviment: Int
    let minimumBalanceRemain: Int
    let administrationFee:  Double?
    let performanceFee: Double?
    let dateQuota: String
    let valueQuota: Double
    let numberOfDaysFinancialInvestment: Int
    let investimentQuota: String
    let investmentFinancialSettlement: String
    let rescueQuota: String
    let rescuefinancialSettlement: String
    let hourInvestimentRescue: String
    let anbimaRating: String
    let anbimaCode :  String?
    let categoryDescription: String
    let categoryCode: Int
    let subCategoryDescription:   Int?
    let subCategoryCode:   Int?
    let custody: String
    let auditing : String?
    let manager: String
    let administrator: String
    let updateDate:  String?
    let files:[File]
    let quotaRule:   Int?
    
    private enum CodingKeys: String, CodingKey {
        case minimumInitialInvestment =  "minimumInitialInvestment"
        case minimumMoviment =  "minimumMoviment"
        case minimumBalanceRemain =  "minimumBalanceRemain"
        case administrationFee = "administrationFee"
        case performanceFee = "performanceFee"
        case dateQuota =  "dateQuota"
        case valueQuota = "valueQuota"
        case numberOfDaysFinancialInvestment =  "numberOfDaysFinancialInvestment"
        case investimentQuota =  "investimentQuota"
        case investmentFinancialSettlement =  "investmentFinancialSettlement"
        case rescueQuota =  "rescueQuota"
        case rescuefinancialSettlement =  "rescuefinancialSettlement"
        case hourInvestimentRescue =  "hourInvestimentRescue"
        case anbimaRating =  "anbimaRating"
        case anbimaCode = "anbimaCode"
        case categoryDescription =  "categoryDescription"
        case categoryCode =  "categoryCode"
        case subCategoryDescription = "subCategoryDescription"
        case subCategoryCode = "subCategoryCode"
        case custody =  "custody"
        case auditing = "auditing"
        case manager =  "manager"
        case administrator =  "administrator"
        case updateDate = "updateDate"
        case files = "files"// ver se e vdd
        case quotaRule = "fquotaRule"
      
    }
}
struct File: Decodable {
    let id: Int
    let typeId: Int
    let version: String?
    let name: String
    let format: String
    let description: String
    let typeCode: String
    let size:   Int?
    let mimeType:   Int?
    let file:   Int?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case typeId = "typeId"
        case version = "version"
        case name = "name"
        case format = "format"
        case description = "description"
        case typeCode = "typeCode"
        case size = "size"
        case mimeType = "mimeType"
        case file = "file"
        
    }
}
struct Investimento: Decodable {
    let mainDisclaimer: String
    let confirmationDisclaimer: String
    let requestDate: String
    let aciValidDate: String
    let value:   Int?
    let maxInvestmentDate: String
    let quotaDate:   Int?
    let settlementDate:   Int?
    let aciErrorCode:   Int?
    
    private enum CodingKeys: String, CodingKey {
        case mainDisclaimer = "mainDisclaimer"
        case confirmationDisclaimer = "confirmationDisclaimer"
        case requestDate = "requestDate"
        case aciValidDate = "aciValidDate"
        case value =  "value"
        case maxInvestmentDate = "maxInvestmentDate"
        case quotaDate =  "uotaDate"
        case settlementDate =  "settlementDate"
        case aciErrorCode =  "aciErrorCode"
        
    }
}
