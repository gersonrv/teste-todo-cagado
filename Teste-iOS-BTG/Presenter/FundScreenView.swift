//
//  ViewController.swift
//  Teste-iOS-BTG
//
//  Created by BTG Pactual digital on 30/11/18.
//  Copyright © 2018 BTG Pactual. All rights reserved.
//

import UIKit
var  filteredData:[Funds] = []
class FundScreenView: UIViewController {

    
    var reload = false
    var dateCellExpanded = false
    var index = 0
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var colorConservador: UIView!
    @IBOutlet weak var conservador: UILabel!
    @IBOutlet weak var colorModerado: UIView!
    @IBOutlet weak var moderado: UILabel!
    @IBOutlet weak var colorSofisticado: UIView!
    @IBOutlet weak var sofisticado: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var navigationbar: UINavigationBar!
    @IBOutlet weak var searchBar: UITextField!
    
    
    
    
    var search = ""
   
    //mark: Load view
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadStaticElements()
        self.loadData()
        self.searchBar.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         dateCellExpanded = false
         index = 0
        tableView.reloadData()
    }
    func loadStaticElements(){
        self.customizeSearchBar()
        
        
        self.conservador.text = Labels.share.conservador
        self.moderado.text = Labels.share.moderado
        self.sofisticado.text = Labels.share.sofisticado
        
        self.colorConservador.backgroundColor = UIColor.btg_blue
        self.colorModerado.backgroundColor = UIColor.btg_orange
        self.colorSofisticado.backgroundColor = UIColor.btg_red
        
        self.conservador.textColor = UIColor.label_grey
        self.sofisticado.textColor = UIColor.label_grey
        self.moderado.textColor = UIColor.label_grey
    
        self.mainView.backgroundColor = UIColor.btg_grey
        self.navigationbar.barTintColor = UIColor.btg_grey
        
        
        
    }
    
    func customizeSearchBar(){
        self.searchBar.leftViewMode = .always
        self.searchBar.leftView = UIImageView(image: UIImage(named: "search_icon"))
        self.searchBar.setBottonLine(lineColor: UIColor.lineColor)
    }
    
    func loadData(){
        NetworkManager.shared.request_funds {
            self.reload = true
            filteredData = dataFund
            self.reloadDataFromModel()
        }
    }
    @IBAction func settingsFilter(_ sender: Any) {
        let nextView = (
            storyboard?.instantiateViewController(
                withIdentifier: "settings")
            )!
        //nextView.view.backgroundColor = UIColor.clear
        nextView.modalTransitionStyle = .crossDissolve
        present(nextView, animated: true, completion: nil)
    }
    
    


}
extension FundScreenView: UITableViewDelegate, UITableViewDataSource{
    
    func reloadDataFromModel(_ refreshTable: Bool = true){
        
        if refreshTable{
            self.tableView.reloadData()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !reload{
            return 0
        }else{
            return filteredData.count
        }
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == index{
            if dateCellExpanded {
                return 395
            } else {
                return 140
            }
        }
            return 140
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! FundsCell
        let data = filteredData[indexPath.row]
        cell.data = data
        
        
     if !self.dateCellExpanded {
        cell.closeButton.addAction(for: .touchUpInside) {
             print("Tocou dentro")
                self.index = indexPath.row
                cell.closeButton.setImage(UIImage(named: "error"), for: .normal)
                cell.opstionDetailView.isHidden = false
                self.dateCellExpanded = true
            tableView.beginUpdates()
            tableView.endUpdates()
            self.reloadDataFromModel()
        }
     }else{
        cell.closeButton.addAction(for: .touchUpInside) {
                self.index = indexPath.row
                cell.closeButton.setImage(UIImage(named: "plud-1"), for: .normal)
               cell.opstionDetailView.isHidden = true
                self.dateCellExpanded = false
            tableView.beginUpdates()
            tableView.endUpdates()
            self.reloadDataFromModel()
          }
        }
    

        return cell
    }
    
}

extension FundScreenView: UITextFieldDelegate{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        

        filteredData.removeAll()
        if searchBar.text?.count != 0{
            for produc in dataFund {
                if let producSearch = searchBar.text {
                let range = produc.product.lowercased().range(of: producSearch, options: .caseInsensitive, range: nil, locale:  nil)
                    if range != nil{
                        filteredData.append(produc)
                    }
                }
            }
        }else{
            for product in dataFund{
                filteredData.append(product)
            }
        }
    
       self.tableView.reloadData()
        return true
    }
}
