//
//  FundsCell.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 05/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import UIKit

class FundsCell: UITableViewCell {
    
    @IBOutlet weak var product: UILabel!
    @IBOutlet weak var categoryDescription: UILabel!
    @IBOutlet weak var dozeMeses: UILabel!
    @IBOutlet weak var twelveMonths: UILabel!
    @IBOutlet weak var aplicacaoInicial: UILabel!
    @IBOutlet weak var resgateEm: UILabel!
    @IBOutlet weak var minimumInitialInvestiment: UILabel!
    @IBOutlet weak var rescueFinancialSttlement: UILabel!
    @IBOutlet weak var colorRisk: UIView!

    
    
    //Dados da cell expandida
    @IBOutlet weak var mesAtual: UILabel!
    @IBOutlet weak var ano: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var inicioDoFundo: UILabel!
    @IBOutlet weak var patrimonioPublico: UILabel!
    @IBOutlet weak var begin: UILabel!
    @IBOutlet weak var netEquity: UILabel!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var opstionDetailView: UIView!
    
   
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var button: UIButton!
    
    
    var data: Funds!{
        didSet{
            fillData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.colorLabe()
        self.opstionDetailView.isHidden = true
    
    }
    
    func fillData(){
        self.product.text = data.product
        self.categoryDescription.text = data.detail.categoryDescription
        if data.profitability.twelveMonths == nil{
          self.twelveMonths.text = ""
        }else{
            self.twelveMonths.text = formatNumber.share.formartPercent(number: data.profitability.twelveMonths!)
        }
        
        self.dozeMeses.text = Labels.share.dozeMeses
        self.aplicacaoInicial.text = Labels.share.aplicacaoInicial
        self.resgateEm.text = Labels.share.resgateEm
        self.minimumInitialInvestiment.text = formatNumber.share.formatLargNumber(number: data.detail.minimumInitialInvestment)
        self.rescueFinancialSttlement.text = data.detail.rescuefinancialSettlement
        self.colorRisk.backgroundColor = colorRiskFunction()
        
        //cell expandida
        self.ano.text = Labels.share.ano
        self.mesAtual.text = Labels.share.mesAtual
        self.month.text = formatNumber.share.formartPercent(number: data.profitability.month)
        if data.profitability.year == nil{
            self.year.text = ""
        }else{
            self.year.text = formatNumber.share.formartPercent(number: data.profitability.year!)
        }
        self.inicioDoFundo.text = Labels.share.inicioDoFundo
        self.begin.text = data.begin.formatStringDate()
        self.patrimonioPublico.text = Labels.share.patrimonioLiquido
        self.netEquity.text = formatNumber.share.formatLargNumber(number: data.netEquity)
        
    }
    
    func colorRiskFunction()-> UIColor{
        switch data.riskName {
        case "CONSERVADOR":
            return UIColor.btg_blue
        case "MODERADO":
            return UIColor.btg_orange
        case "SOFISTICADO":
            return UIColor.btg_red
        default:
            return UIColor.black
        }
        
        
    }
    
    func colorLabe(){
        self.product.textColor = UIColor.label_grey
        self.categoryDescription.textColor = UIColor.label_grey
        self.dozeMeses.textColor = UIColor.label_grey
        self.aplicacaoInicial.textColor = UIColor.label_grey
        self.resgateEm.textColor = UIColor.label_grey
        self.minimumInitialInvestiment.textColor = UIColor.label_grey
        self.rescueFinancialSttlement.textColor = UIColor.label_grey
        self.colorRisk.backgroundColor = UIColor.label_grey
        
        self.month.textColor = UIColor.label_grey
        self.twelveMonths.textColor = UIColor.label_grey
        self.mesAtual.textColor = UIColor.label_grey
        self.ano.textColor = UIColor.label_grey
        self.year.textColor = UIColor.label_grey
        self.inicioDoFundo.textColor = UIColor.label_grey
        self.patrimonioPublico.textColor = UIColor.label_grey
        self.begin.textColor = UIColor.label_grey
        self.netEquity.textColor = UIColor.label_grey
    }
    
   
    
    

}
class ClosureSleeve {
    let closure: ()->()
    
    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }
    
    @objc func invoke () {
        closure()
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControl.Event, _ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, String(format: "[%d]", arc4random()), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}
