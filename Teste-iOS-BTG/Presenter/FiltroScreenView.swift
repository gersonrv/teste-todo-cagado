//
//  Filtros.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 07/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import Foundation
import UIKit
var verificarConservador = true
var verificarModerado = true
var verificarSofisticado = true
class FiltroScreeView: UIViewController{
    
    //label
    
    @IBOutlet weak var filterTitle: UILabel!
    @IBOutlet weak var conservador: UILabel!
    @IBOutlet weak var moderado: UILabel!
    @IBOutlet weak var sofisticado: UILabel!
    @IBOutlet weak var filtarLabel: UIButton!
    
    @IBOutlet weak var categoriaLabel: UILabel!
    @IBOutlet weak var aplicacaoMaximaLabel: UILabel!
    @IBOutlet weak var resgateLabel: UILabel!
    @IBOutlet weak var gestorLabel: UILabel!
    @IBOutlet weak var ordenarLabel: UILabel!
    
    @IBOutlet weak var categoria: UITextField!
    @IBOutlet weak var aplicacaoMaxima: UITextField!
    @IBOutlet weak var resgateEm: UITextField!
    @IBOutlet weak var gestor: UITextField!
    @IBOutlet weak var ordenarPor: UITextField!
    
    @IBOutlet weak var colorConservador: UIView!
    @IBOutlet weak var colorModerado: UIView!
    @IBOutlet weak var colorSofisticado: UIView!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var checkConservador: UIButton!
    @IBOutlet weak var checkModerado: UIButton!
    @IBOutlet weak var checkSofisticado: UIButton!
    @IBOutlet weak var bakcgroundNavigation: UINavigationBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadStaticElements()
        self.customizeTextFiel()
        self.loadCheckBox()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.loadCheckBox()
    }
    
    func loadStaticElements(){
        self.filterTitle.text = Labels.share.filtrarSuaBusca
        self.filtarLabel.tintColor = UIColor.label_grey
        self.conservador.text = Labels.share.conservador
        self.moderado.text = Labels.share.moderado
        self.sofisticado.text = Labels.share.sofisticado
        
        self.categoriaLabel.text = Labels.share.categoria
        self.aplicacaoMaximaLabel.text = Labels.share.aplicacaoMaxima
        self.resgateLabel.text = Labels.share.resgateEm
        self.gestorLabel.text = Labels.share.gestor
        self.ordenarLabel.text = Labels.share.ordenadoPor
        
        self.categoriaLabel.textColor = UIColor.label_grey
        self.aplicacaoMaximaLabel.textColor = UIColor.label_grey
        self.resgateLabel.textColor = UIColor.label_grey
        self.gestorLabel.textColor = UIColor.label_grey
        self.ordenarLabel.textColor = UIColor.label_grey
        
        self.filtarLabel.setTitle(Labels.share.filtrar, for: .normal)
    
        self.colorConservador.backgroundColor = UIColor.btg_blue
        self.colorModerado.backgroundColor = UIColor.btg_orange
        self.colorSofisticado.backgroundColor = UIColor.btg_red
        
        self.navigationBar.barTintColor = UIColor.btg_grey
        
        self.bakcgroundNavigation.barTintColor = UIColor.btg_grey
        self.navigationBar.layer.cornerRadius = 10
        self.navigationBar.clipsToBounds = true
       
        self.viewFilter.layer.cornerRadius = 10
       
        
    }
    func customizeTextFiel(){
        self.categoria.placeholder = Labels.share.todos
        self.aplicacaoMaxima.placeholder = Labels.share.todos
        self.resgateEm.placeholder = Labels.share.todos
        self.gestor.placeholder = Labels.share.todos
        self.ordenarPor.placeholder = Labels.share.nenhum
        
        self.categoria.setBottonLine(lineColor: UIColor.lineColor)
        self.aplicacaoMaxima.setBottonLine(lineColor: UIColor.lineColor)
        self.resgateEm.setBottonLine(lineColor: UIColor.lineColor)
        self.gestor.setBottonLine(lineColor: UIColor.lineColor)
        self.ordenarPor.setBottonLine(lineColor: UIColor.lineColor)
    }
    
    func loadCheckBox(){
        if  verificarConservador {
            checkConservador.setImage(UIImage(named: "checkbox"), for: .normal)
        }else{
            checkConservador.setImage(UIImage(named: "check-box-empty"), for: .normal)
        }
        if  verificarModerado {
            checkModerado.setImage(UIImage(named: "checkbox"), for: .normal)
        }else{
            checkModerado.setImage(UIImage(named: "check-box-empty"), for: .normal)
        }
        if verificarSofisticado {
            checkSofisticado.setImage(UIImage(named: "checkbox"), for: .normal)
        }else{
            checkSofisticado.setImage(UIImage(named: "check-box-empty"), for: .normal)
        }
    }
    
    @IBAction func checkBoxConservador(_ sender: Any) {
        if verificarConservador{
            checkConservador.setImage(UIImage(named: "check-box-empty"), for: .normal)
            verificarConservador = false
            PreencherArray.shared.verificarDesmarcarConservador(conservador: verificarConservador, moderado: verificarModerado, sofisticado: verificarSofisticado)
        }else{
            checkConservador.setImage(UIImage(named: "checkbox"), for: .normal)
            verificarConservador = true
             PreencherArray.shared.verificarMarcarModerado(conservador: verificarConservador, moderado: verificarModerado, sofisticado: verificarSofisticado)
        }
    }
    
    @IBAction func checkBoxModerado(_ sender: Any) {
        if verificarModerado{
            checkModerado.setImage(UIImage(named: "check-box-empty"), for: .normal)
            verificarModerado = false
            PreencherArray.shared.verificarDesmarcarModerado(conservador: verificarConservador, moderado: verificarModerado, sofisticado: verificarSofisticado)
        }else{
            checkModerado.setImage(UIImage(named: "checkbox"), for: .normal)
            verificarModerado = true
            PreencherArray.shared.verificarMarcarModerado(conservador: verificarConservador, moderado: verificarModerado, sofisticado: verificarSofisticado)
        }
    }
    
    @IBAction func checkBoxSofisticado(_ sender: Any) {
        if verificarSofisticado{
            checkSofisticado.setImage(UIImage(named: "check-box-empty"), for: .normal)
            verificarSofisticado = false
            PreencherArray.shared.verificarDesmarcarSofisticado(conservador: verificarConservador, moderado: verificarModerado, sofisticado: verificarSofisticado)
        }else{
            checkSofisticado.setImage(UIImage(named: "checkbox"), for: .normal)
            verificarSofisticado = true
             PreencherArray.shared.verificarMarcarSofisticado(conservador: verificarConservador, moderado: verificarModerado, sofisticado: verificarSofisticado)
        }
    }
    
    @IBAction func filtrarAction(_ sender: Any) {
      dismiss(animated: true, completion: nil)
    
    }
    
    @IBAction func voltar(_ sender: Any) {
        //Cancelar
        filteredData.removeAll()
        FiltrarCategoria.shared.adicionarTodos()
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func limpar(_ sender: Any) {

         verificarConservador = true
         verificarModerado = true
         verificarSofisticado = true
         filteredData.removeAll()
         FiltrarCategoria.shared.adicionarTodos()
         checkConservador.setImage(UIImage(named: "checkbox"), for: .normal)
         checkModerado.setImage(UIImage(named: "checkbox"), for: .normal)
         checkSofisticado.setImage(UIImage(named: "checkbox"), for: .normal)
    }
    
    
}
