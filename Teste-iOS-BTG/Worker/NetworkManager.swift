//
//  NetworkManager.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 04/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import UIKit
import Alamofire
var dataFund: [Funds]!
class NetworkManager{
    static let shared = NetworkManager()
    let endPoint = "https://www.btgpactualdigital.com/services/public/funds/"
    
    func request_funds(_ completionHanlder: @escaping ()-> Void){
        Alamofire.request(endPoint).responseJSON { (response) in
            let data = response.data!
            
            do{
                let DATA = try JSONDecoder().decode([Funds].self, from: data)
                dataFund = DATA
                completionHanlder()
            }catch let error{
                print("Erro: ", error )
            }
        }
    }
}
