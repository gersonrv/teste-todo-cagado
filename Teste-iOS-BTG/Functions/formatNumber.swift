//
//  formatNumber.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 07/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import Foundation
import UIKit

class formatNumber{

    static let share = formatNumber()
    func formatLargNumber(number: Int)->String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        let value = formatter.string(from: NSNumber(value: number))
        
        return value!
    }
    func formatLargNumber(number: Double)->String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        let value = formatter.string(from: NSNumber(value: number))
        
        return value!
    }
    
    func formartPercent(number: Double)-> String{
        return String(format: "%.2f", number) + "%"
    }
}
