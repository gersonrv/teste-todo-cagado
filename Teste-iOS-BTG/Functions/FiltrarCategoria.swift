//
//  FiltrarCategoria.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 08/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import Foundation
import  UIKit
class FiltrarCategoria{
    static let shared = FiltrarCategoria()
    func filtrarConservador(){
        for item in dataFund{
            if item.riskName == "CONSERVADOR"{
                filteredData.append(item)
            }
        }
        
    }
    
    func filtrarModerado(){
        for item in dataFund{
            if item.riskName == "MODERADO"{
                filteredData.append(item)
            }
        }
    }
    
    func filtrarSofisticado(){
        for item in dataFund{
            if item.riskName == "SOFISTICADO"{
                filteredData.append(item)
            }
        }
    }
    
    func adicionarTodos(){
        for item in dataFund{
                filteredData.append(item)
            
        }
    }
}
