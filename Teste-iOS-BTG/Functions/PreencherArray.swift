//
//  PreencherArray.swift
//  Teste-iOS-BTG
//
//  Created by Gerson Rodrigo on 08/02/19.
//  Copyright © 2019 BTG Pactual. All rights reserved.
//

import Foundation
import UIKit

class PreencherArray {
    
    static let shared = PreencherArray()
    func verificarDesmarcarConservador(conservador: Bool, moderado: Bool, sofisticado: Bool){
        filteredData.removeAll()
        if conservador == false && moderado == true && sofisticado == true {
            FiltrarCategoria.shared.filtrarModerado()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else if conservador == false && moderado == true && sofisticado == false {
            FiltrarCategoria.shared.filtrarModerado()
        }else if conservador == false && moderado == false && sofisticado == true{
            FiltrarCategoria.shared.filtrarSofisticado()
        }
    }
    
    
    func verificarDesmarcarModerado(conservador: Bool, moderado: Bool, sofisticado: Bool){
        filteredData.removeAll()
        if conservador == true && moderado == false && sofisticado == true {
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else if conservador == true && moderado == false && sofisticado == false {
            FiltrarCategoria.shared.filtrarConservador()
        }else if conservador == false && moderado == false && sofisticado == true{
            FiltrarCategoria.shared.filtrarSofisticado()
        }
    }
    
    
    func verificarDesmarcarSofisticado(conservador: Bool, moderado: Bool, sofisticado: Bool){
        filteredData.removeAll()
        if conservador == true && moderado == true && sofisticado == false{
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarModerado()
        }else if conservador == true && moderado == false && sofisticado == false {
            FiltrarCategoria.shared.filtrarConservador()
        }else if conservador == false && moderado == true && sofisticado == false{
            FiltrarCategoria.shared.filtrarModerado()
        }
    }
    
    
    func verificarMarcarConservador(conservador: Bool, moderado: Bool, sofisticado: Bool){
        filteredData.removeAll()
        if conservador == true && moderado == true && sofisticado == true {
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarModerado()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else if conservador == true && moderado == true && sofisticado == false {
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarModerado()
        }else if conservador == true && moderado == false && sofisticado == true{
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else{
            FiltrarCategoria.shared.filtrarConservador()
        }
    }
    
    
    func verificarMarcarModerado(conservador: Bool, moderado: Bool, sofisticado: Bool){
        filteredData.removeAll()
        if conservador == true && moderado == true && sofisticado == true {
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarModerado()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else if conservador == true && moderado == true && sofisticado == false {
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarModerado()
        }else if conservador == false && moderado == true && sofisticado == true{
            FiltrarCategoria.shared.filtrarModerado()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else{
            FiltrarCategoria.shared.filtrarModerado()
        }
    }
    
    
    func verificarMarcarSofisticado(conservador: Bool, moderado: Bool, sofisticado: Bool){
        filteredData.removeAll()
        if conservador == true && moderado == true && sofisticado == true {
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarModerado()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else if conservador == true && moderado == false && sofisticado == true {
            FiltrarCategoria.shared.filtrarConservador()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else if conservador == false && moderado == true && sofisticado == true{
            FiltrarCategoria.shared.filtrarModerado()
            FiltrarCategoria.shared.filtrarSofisticado()
        }else{
            FiltrarCategoria.shared.filtrarSofisticado()
        }
    }
    
    
}
